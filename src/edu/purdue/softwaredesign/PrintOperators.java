package edu.purdue.softwaredesign;

/**
 * Prints all operators
 * 
 * @author Piyush Kochhar
 *
 */
public class PrintOperators extends Calculator {
  /**
   *@return Print all operations
   */
  public static void print() {
    for (int i = 0; i < allOperators.length; i++) {
      System.out.print(allOperators[i] + " ");
    }
  }
  
}
