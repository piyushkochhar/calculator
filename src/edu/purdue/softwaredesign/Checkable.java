package edu.purdue.softwaredesign;

/**
 * Checks if the operation is available
 * 
 * @author Piyush Kochhar
 *
 */
public interface Checkable {
  
  /**
   * 
   * @param operator Type of operator
   * @param allOperators A list of all operations available
   * @return boolean value if the operator exists
   */
  public static boolean checkOperator(char operator, char[] allOperators) {
    boolean state = false;
    for (int i = 0; i < allOperators.length; i++) {
      if (operator == allOperators[i]) {
        state = true;
        break;
      }
    }
    return state;
  }
}
