package edu.purdue.softwaredesign;

import java.util.Scanner;

/**
 * Performs operations on operators.
 * 
 * @author Piyush Kochhar
 * 
 **/
public class Calculator implements Operatable, Checkable {
  
  static char[] allOperators = {'+', '-', '*'};
  
  static double result = 0.0;
  
  static char operator;
  
  @Override
  /**
   * return Formatted result
   */
  public String toString() {
    return String.format("Result is : " + result);
  }
  
  public static void main(String[] args) {
    
    Calculator calc = new Calculator();
    
    while (true) {
      Scanner sc = new Scanner(System.in);
      
      System.out.print("Operators are : ");
      
      PrintOperators.print();
      
      System.out.println(System.lineSeparator() + "Enter your choice or Press 'q' to Quit");
      
      operator = sc.nextLine().charAt(0);
      
      if (operator == 'q') {
        System.out.println("Program is Quit");
        break;
      }
      
      if (Checkable.checkOperator(operator, allOperators) == true) {
        
        System.out.println("Enter Number 1 : ");
        double num1 = sc.nextDouble();
        
        System.out.println("Enter Number 2 : ");
        double num2 = sc.nextDouble();
        
        switch (operator) {
          case '+':
            result = Operatable.add(num1, num2);
            break;
          case '-':
            result = Operatable.sub(num1, num2);
            break;
          case '*':
            result = Operatable.mul(num1, num2);
            break;
        }
      } else {
        throw new Error("Invalid Operator");
      }
      
      System.out.println(calc);
      
    }
  }
}
