package edu.purdue.softwaredesign;

/**
 * Performs operations on operands
 * 
 * @author Piyush Kochhar
 * 
 **/
public interface Operatable {
  
  /**
   * 
   * @param a Value of 1st operand
   * @param b Value of 2st operand
   * @return Sum of both the operands
   */
  public static double add(double a, double b) {
    return a + b;
  }
  
  /**
   * 
   * @param a Value of 1st operand
   * @param b Value of 2st operand
   * @return Difference of both the operands
   */
  public static double sub(double a, double b) {
    return a - b;
  }
  
  /**
   * 
   * @param a Value of 1st operand
   * @param b Value of 2st operand
   * @return Product of both the operands
   */
  public static double mul(double a, double b) {
    return a * b;
  }
  
}
